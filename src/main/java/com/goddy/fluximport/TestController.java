package com.goddy.fluximport;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ZeroCopyHttpOutputMessage;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

/**
 * Created by Goddy on 2018/11/9.
 */
@RestController
public class TestController {

  private String fileName = "测试.xlsx";

  @GetMapping("/download/resource")
  public Mono<Void> test1(ServerHttpResponse response) {
    ClassPathResource resource = new ClassPathResource(fileName);
    try {
      File file = resource.getFile();
      return downloadFile(response, file, fileName);
    } catch (IOException e) {
      return Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND, "未找到相应文件！"));
    }
  }

  @GetMapping("/download/excel")
  public Mono<Void> test2(ServerHttpResponse response) {
    XSSFWorkbook workbook = new XSSFWorkbook();
    Sheet sheet = workbook.createSheet("sheet-1");
    sheet.createRow(0).createCell(0).setCellValue("你好啊");
    sheet.createRow(2).createCell(2).setCellValue("nico");
    return Mono.fromCallable(() -> {
      File file = new File(fileName);
      workbook.write(new FileOutputStream(file));
      return file;
    }).flatMap(file -> downloadFile(response, file, fileName));
  }

  @PostMapping("/upload")
  public Mono<String> test3(@RequestPart FilePart filePart) {
    return filePart2file(filePart).flatMap(file -> {
      if (StringUtils.endsWithIgnoreCase(file.getName(), "xls") || StringUtils
          .endsWithIgnoreCase(file.getName(), "xlsx")) {
        return Mono.fromCallable(() -> getWorkBook(file))
            .onErrorMap(e -> new ResponseStatusException(HttpStatus.NOT_FOUND, "上传失败！"))
            .map(workbook -> workbook.getSheetAt(0).getRow(0).getCell(0).getStringCellValue());
      } else {
        return Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND, "上传失败！"));
      }
    });
  }

  private Mono<Void> downloadFile(ServerHttpResponse response, File file, String fileName) {
    ZeroCopyHttpOutputMessage zeroCopyHttpOutputMessage = (ZeroCopyHttpOutputMessage) response;
    try {
      response.getHeaders()
          .set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=".concat(
              URLEncoder.encode(fileName, StandardCharsets.UTF_8.displayName())));
      return zeroCopyHttpOutputMessage.writeWith(file, 0, file.length());
    } catch (UnsupportedEncodingException e) {
      throw new UnsupportedOperationException();
    }
  }

  private Workbook getWorkBook(File file) throws IOException {
    FileInputStream fileInputStream = new FileInputStream(file);
    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    IOUtils.copy(fileInputStream, byteArrayOutputStream);
    Workbook workbook = null;
    try {
      POIFSFileSystem fs = new POIFSFileSystem(file);
      workbook = new HSSFWorkbook(fs);
    } catch (Exception e) {
      workbook = new XSSFWorkbook(new ByteArrayInputStream(byteArrayOutputStream.toByteArray()));
    }
    return workbook;
  }

  private Mono<File> filePart2file(FilePart filePart) {
    return Mono.fromCallable(() -> {
      Path path = Files.createTempFile(null, filePart.filename());
      File file = path.toFile();
      filePart.transferTo(file);
      return file;
    }).onErrorMap(error -> new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "读取文件失败！"));
  }
}
